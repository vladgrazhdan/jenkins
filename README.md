# Jenkins configuration files

## Intro

Jenkins configuration files with a shared library to deploy CI/CD pipelines.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* Jenkins v2.333+
