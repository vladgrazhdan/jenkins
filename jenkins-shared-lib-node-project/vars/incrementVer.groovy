#!/usr/bin/env groovy

def call(String VERSION) {
    echo 'incrementing project version...'
    sh "npm version ${VERSION}"
    def match = readFile('package.json') =~ '"version": "(.+)"'
    echo "match value is ${match}"
    def version = match[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}