#!/usr/bin/env groovy

def call() {
    echo 'testing the app...'
    sh 'npm install'
    sh 'npm run test'
}
