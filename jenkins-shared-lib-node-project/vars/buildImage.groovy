#!/usr/bin/env groovy

def call(String IMAGE_NAME) {
    echo 'building the docker image...'
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t repo/my-repo:$IMAGE_NAME ."
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh "docker push repo/my-repo:$IMAGE_NAME"
    }
}